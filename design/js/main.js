let first_strip = document.querySelector("#just").children[0],
	second_strip = document.querySelector("#just").children[1],
	third_strip = document.querySelector("#just").children[2];




$("#burger").mouseenter(function() {
	$("#burger").animate({backgroundColor:"#b6814f"},350)

	$("#hover_wrapper").css('display', 'block');
	
	dynamics.animate(first_strip, {
		translateX: -33,
		scale: 1,
  	
		}, {
		 type: dynamics.gravity,
		  bounciness:0,
		  elasticity:1000,
		  duration: 100
	})

	dynamics.animate(second_strip, {
		translateX: 33,
		scale: 1,
 
		}, {
		  type: dynamics.gravity,
		  bounciness:0,
		  elasticity:1000,
		  duration: 100
	})

	dynamics.animate(third_strip, {
		translateX: -33,
		scale: 1,
 
		}, {
		  type: dynamics.gravity,
		  bounciness:0,
		  elasticity:1000,
		  duration: 100
		})
	})



$("#burger").mouseleave(function() {
	$("#burger").animate({backgroundColor:"#fff"},150)

	$("#hover_wrapper").css('display', 'none');

	dynamics.animate(first_strip, {
		translateX: 0,
		scale: 1,
 
		}, {
		  type: dynamics.spring,
		  frequency: 800,
		  friction: 300,
		  duration: 1500
	})

	dynamics.animate(second_strip, {
		translateX: 0,
		scale: 1,	
 
		}, {
		  type: dynamics.spring,
		  frequency: 800,
		  friction: 300,
		  duration: 1500
	})

	dynamics.animate(third_strip, {
		translateX: 0,
		scale: 1,
 
		}, {
		  type: dynamics.spring,
		  frequency: 800,
		  friction: 300,
		  duration: 1500
	})

})


$("#burger").click(() => {
	$(".main_menu").fadeIn();
})

$(".close_icon").click(() => {
	$(".main_menu").fadeOut();
})


$("#second_part_bars_lower_shoes").mouseenter(() => {
	$("#second_part_bars_lower_shoes").animate({backgroundColor:"#b6814f",color:"#ebded3"},200)
	$("#shoes_submenu").fadeIn(200);
})

$("#shoes_submenu").mouseleave(() => {
	$("#second_part_bars_lower_shoes").animate({backgroundColor:"#f4f4f4",color:"#000"},200)
	$("#shoes_submenu").fadeOut(200);
})

//------------------------------------------------

$("#second_part_bars_lower_accessories").mouseenter(() => {
		$("#second_part_bars_lower_accessories").animate({backgroundColor:"#b6814f",color:"#ebded3"},200)
		$("#accesories_submenu").fadeIn(200);
})

$("#second_part_bars_lower_accessories").mouseleave(() => {
	$("#second_part_bars_lower_accessories").animate({backgroundColor:"#f4f4f4",color:"#000"},200)
	$("#accesories_submenu").fadeOut(200);

})

//--------------------------------------------------

$("#second_part_bars_lower_bags").mouseenter(() => {
		$("#second_part_bars_lower_bags").animate({backgroundColor:"#b6814f",color:"#ebded3"},200)
		$("#bags_submenu").fadeIn(200);
})

$("#second_part_bars_lower_bags").mouseleave(() => {
	$("#second_part_bars_lower_bags").animate({backgroundColor:"#f4f4f4",color:"#000"},200)
	$("#bags_submenu").fadeOut(200);

})



$("#city").click(() => {
	
	$("#city").css('zIndex','10')

	$("#logo").css('zIndex','10')

	$(city_choose_menu).fadeIn(350);

})




$("#city_close_icon").click(() => {
	$("#city").css('zIndex','0')
	$("#logo").css('zIndex','0')
	$(".city_choose_menu").fadeOut(350);
})


$("#another_city_wrapper").click(() => {
	
	$("#city").css('zIndex','10')
	$("#logo").css('zIndex','10')

	$(".city_choose_menu").fadeIn(350);

})


$("#account").click(() => {
	$("#logo").css('zIndex','10');
	$("#account").css('zIndex','10');
	$("#account_menu").fadeIn(350);
})



$("#account_close_icon").click(() => {
	$("#logo").css('zIndex','0');
	$("#account").css('zIndex','0');
	$("#account_menu").fadeOut(350);
})


$("#cart")
	.on("click",cart_mouse_click)
	.on("mouseenter",cart_mouse_enter)
	.on("mouseleave",cart_mouse_leave);

function cart_mouse_enter() {
	$("#black_cart_icon").fadeOut(0);

	$("#cart").animate({
		backgroundColor:"#b6814f",
		color:"white",
		float:"left"
	},350);
	
	$("#white_cart_icon").fadeIn(350);
}

function cart_mouse_leave() {
	$("#white_cart_icon").fadeOut(0);

	$("#cart").animate({
		backgroundColor:"#fff",
		color:"#000",
		float:"left"
	},350);

	$("#black_cart_icon").fadeIn(350);
}



function cart_mouse_click() {
	

	$("#cart").css('position','absolute');
	$("#cart").css('left','93%');
	$("#cart").css('display','flex');
	$("#cart").css('zIndex','11');

	
	$("#black_cart_icon").css('display','none');
	$(white_cart_icon).fadeIn(350);

	$("#cart").animate({
		left: "0px",
		position:"absolute",
		width:"100%",
		backgroundColor: "#b6814f",
		color:"#fff"

	}, 350)

	$("#cart_word").fadeIn(350);
	$("#cart_menu").fadeIn(350);
	$("#close_icon_in_cart").fadeIn(350);

	$("#cart")
		.off("click", cart_mouse_click)
		.off("mouseenter",cart_mouse_enter)
		.off("mouseleave",cart_mouse_leave);

	$("#close_icon_in_cart").on("click",close_cart_menu);	

}





function close_cart_menu() {

	$("#cart").css('zIndex','1');

	$("#cart").animate({
		left: "92%",
		
		width:"100px",
		backgroundColor: "#fff",
		color:"#000"
	},350)

	$("#cart_menu").fadeOut(350);
	$("#cart_word").fadeOut(350);

	$("#cart")
	// .on("click", cart_mouse_click)   //Почему-то эта функция вызывается без клика
	.on("mouseenter",cart_mouse_enter)
	.on("mouseleave",cart_mouse_leave);



	$(white_cart_icon).fadeOut(0);
	$(black_cart_icon).fadeIn(350);

	$("#close_icon_in_cart").off("click",close_cart_menu);
}



$("#lupa").click(() => {
	if($(window).width() <= 960) {

		$("#search_cancel").fadeIn(0);
		$("#search_find").fadeIn(0);
		$("#search_input").fadeIn(0);

		$("#search_cancel").css('display','flex');

		$("#search_find").css('display','flex');

		$("#search").css('padding-top','0px');

		$("#lupa").css('margin-top','20px');

		$("#search").animate({
			width:"100%",
			backgroundColor:"#f4f4f4",
			height:"60px",
			marginTop:"-14px"
			
		},350)



		$("#lupa").css('height','20px');

		$("#search_cancel").on('click', search_cancel_click);
	}
})

const search_cancel_click = () => {

	$("#search_cancel").fadeOut(0);
	$("#search_find").fadeOut(0);
	$("#search_input").fadeOut(0);
	

	$("#search").css('padding-top','8px');
	$("#search").css('height','28px');

	$("#lupa").css('margin-top','0px');

	$("#search").animate({
			width:"25px",
			
			
			marginTop:"0px"
			
		},250)
}